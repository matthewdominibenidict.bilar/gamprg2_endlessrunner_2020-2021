// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "Components/StaticMeshComponent.h"
#include "RunCharacter.h"

// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	SetRootComponent(StaticMesh);

	
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	//OnActorHit.AddDynamic(this, &APickup::OnHit2);
	//StaticMesh->OnComponentHit.AddDynamic(this, &APickup::OnHit);
	//StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnHit);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnOverlap);
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickup::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *OtherActor->GetName());
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		OnGet(character);
	}
}

void APickup::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		OnGet(character);
	}
}

void APickup::OnHit2(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *OtherActor->GetName());
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		OnGet(character);
	}
}

