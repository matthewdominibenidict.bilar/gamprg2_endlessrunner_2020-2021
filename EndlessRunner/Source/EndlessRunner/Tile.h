// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, class ATile*, Tile);

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* ArrowPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* BoundingBox;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* PickupBoxArea;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	FTransform GetAttachTransform();
	FVector GetRandomPointInBoundingBox(UBoxComponent* WhichBox);
	FTransform GetBox(UBoxComponent* ChosenBox);

	UPROPERTY(EditAnywhere, Category = "ObstacleSpawnable")
		TArray<TSubclassOf<class AObstacle>> ObstacleTypes;

	UPROPERTY(EditAnywhere, Category = "ObstacleSpawnable")
		TArray<TSubclassOf<class APickup>> PickupOptions;

	UPROPERTY(BlueprintAssignable)
		FExited TileExit;


	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void SpawnPickup();

	UFUNCTION()
		void WhichToSpawn();

	virtual void Destroyed() override;

private:
	TArray<class AActor*> OSpawns;
	TArray<class AActor*> PSpawns;
};
