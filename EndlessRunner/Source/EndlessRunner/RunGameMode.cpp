// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "TimerManager.h"
#include "RunCharacter.h"
#include "RunCharacterController.h"
#include "GameFramework/Actor.h"
#include "Obstacle.h"


ARunGameMode::ARunGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	for (size_t i = 0; i < 5; i++)
	{
		SpawnTile();
	}
}

void ARunGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARunGameMode::OnTileExit(ATile* tile)
{
	SpawnTile();
	GetWorldTimerManager().SetTimer(timeDelay, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, tile), 0.7f, true);
}

void ARunGameMode::SpawnTile()
{
	FActorSpawnParameters SpawnInfo;
	ATile* tile = GetWorld()->SpawnActor<ATile>(Tile, next, SpawnInfo);
	next = tile->GetAttachTransform();
	tile->TileExit.AddDynamic(this, &ARunGameMode::OnTileExit);
}

void ARunGameMode::DestroyTile(ATile* tiles)
{
	tiles->Destroy();
}