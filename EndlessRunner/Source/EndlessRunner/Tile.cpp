// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ChildActorComponent.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(SceneComponent);

	ArrowPoint = CreateDefaultSubobject<UArrowComponent>("ArrowPoint");
	ArrowPoint->SetupAttachment(SceneComponent);

	BoundingBox = CreateDefaultSubobject<UBoxComponent>("BoundingBox");
	BoundingBox->SetupAttachment(SceneComponent);

	PickupBoxArea = CreateDefaultSubobject<UBoxComponent>("PickupBoxArea");
	PickupBoxArea->SetupAttachment(SceneComponent);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);
	WhichToSpawn();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		TileExit.Broadcast(this);
	}
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters SpawnInfo;
	AObstacle* obs = GetWorld()->SpawnActor<AObstacle>(ObstacleTypes[0], GetBox(BoundingBox), SpawnInfo);
	obs->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	OSpawns.Add(obs);
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters SpawnInfo;
	APickup* picks = GetWorld()->SpawnActor<APickup>(PickupOptions[0], GetBox(PickupBoxArea), SpawnInfo);
	picks->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	PSpawns.Add(picks);
}

void ATile::WhichToSpawn()
{
	int32 randNum = FMath::RandRange(1, 100);

	if (randNum >= 1 && randNum < 50)
	{
		SpawnObstacle();
	}
	else if (randNum >= 50 && randNum <= 100)
	{
		for (size_t i = 0; i < 4; i++)
		{
			SpawnPickup();
		}
	}
}

FTransform ATile::GetAttachTransform()
{
	FTransform attachPoint;
	attachPoint = ArrowPoint->GetComponentTransform();
	return  attachPoint;
}

FVector ATile::GetRandomPointInBoundingBox(UBoxComponent* WhichBox)
{
	FVector Origin = WhichBox->Bounds.Origin;
	FVector BoxExtent = WhichBox->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
}

FTransform ATile::GetBox(UBoxComponent* ChosenBox)
{
	FVector loc = GetRandomPointInBoundingBox(ChosenBox);
	FRotator rot = GetRandomPointInBoundingBox(ChosenBox).Rotation();
	FVector scale = FVector(1.0, 1.0, 1.0);
	return UKismetMathLibrary::MakeTransform(loc, rot, scale);
}

void ATile::Destroyed()
{
	for (AActor* spawnedObject : OSpawns)
	{
		spawnedObject->Destroy();
	}

	for (AActor* spawnPickup: PSpawns)
	{
		spawnPickup->Destroy();
	}
}




