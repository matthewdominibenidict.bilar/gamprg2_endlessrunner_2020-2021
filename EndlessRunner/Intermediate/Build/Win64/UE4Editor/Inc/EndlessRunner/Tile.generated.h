// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ENDLESSRUNNER_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define ENDLESSRUNNER_Tile_generated_h

#define EndlessRunner_Source_EndlessRunner_Tile_h_10_DELEGATE \
struct _Script_EndlessRunner_eventExited_Parms \
{ \
	ATile* Tile; \
}; \
static inline void FExited_DelegateWrapper(const FMulticastScriptDelegate& Exited, ATile* Tile) \
{ \
	_Script_EndlessRunner_eventExited_Parms Parms; \
	Parms.Tile=Tile; \
	Exited.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_SPARSE_DATA
#define EndlessRunner_Source_EndlessRunner_Tile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execWhichToSpawn); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWhichToSpawn); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneComponent() { return STRUCT_OFFSET(ATile, SceneComponent); } \
	FORCEINLINE static uint32 __PPO__ExitTrigger() { return STRUCT_OFFSET(ATile, ExitTrigger); } \
	FORCEINLINE static uint32 __PPO__ArrowPoint() { return STRUCT_OFFSET(ATile, ArrowPoint); } \
	FORCEINLINE static uint32 __PPO__BoundingBox() { return STRUCT_OFFSET(ATile, BoundingBox); } \
	FORCEINLINE static uint32 __PPO__PickupBoxArea() { return STRUCT_OFFSET(ATile, PickupBoxArea); }


#define EndlessRunner_Source_EndlessRunner_Tile_h_12_PROLOG
#define EndlessRunner_Source_EndlessRunner_Tile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_RPC_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_INCLASS \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner_Source_EndlessRunner_Tile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_INCLASS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Tile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNER_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner_Source_EndlessRunner_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
