// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
#ifdef ENDLESSRUNNER_RunGameMode_generated_h
#error "RunGameMode.generated.h already included, missing '#pragma once' in RunGameMode.h"
#endif
#define ENDLESSRUNNER_RunGameMode_generated_h

#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_SPARSE_DATA
#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnTileExit); \
	DECLARE_FUNCTION(execDestroyTile);


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnTileExit); \
	DECLARE_FUNCTION(execDestroyTile);


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public:


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunGameMode)


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Tile() { return STRUCT_OFFSET(ARunGameMode, Tile); }


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_12_PROLOG
#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_INCLASS \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNER_API UClass* StaticClass<class ARunGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner_Source_EndlessRunner_RunGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
